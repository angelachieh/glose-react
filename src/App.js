import React from 'react';
import { Row, Col } from 'react-bootstrap';
import './App.css';

// Container
class App extends React.Component {
  render() {
    return (
      <div>
        <Hero title="Free books" />
        <TitleList />
      </div>
    );
  }
}

// Hero
class Hero extends React.Component {
  render() {
    return (
      <div className="hero">
        <div className="title">{this.props.title}</div>
      </div>
    );
  }
}

// List of titles
class TitleList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {data: []};
  }
  loadContent() {
    var requestUrl = 'https://api.glose.com/v1/booklists/free-books?_test=angela';
    fetch(requestUrl).then((response)=>{
      return response.json();
    }).then((data)=>{
      this.setState({data: data.modules[1].books});
    }).catch((err)=>{
      console.log("There has been an error");
    });
  }
  componentDidMount() {
    this.loadContent();
  }
  render() {
    var titles ='';
    if (this.state.data) {
      titles = this.state.data.map(function(title) {
        return (
          <Item key={title.id} title={title.title} slug={title.slug} coverimage={title.image} />
        );
      });
    }
    return (
      <div className="titleList">
        <Row>
          {titles}
        </Row>
      </div>
    );
  }
}

// Single item in the list of titles
class Item extends React.Component {
  render() {
    return (
      <Col className="item" xs={6} md={2}>
          <a href={"https://glose.com/book/"+this.props.slug+"?list=free-books"}>
            <img src={this.props.coverimage} alt={this.props.title} />
            <div className="title">
              {this.props.title}
            </div>
          </a>
      </Col>
    );
  }
}

export default App;
