# Angela's free books on Glose

## Description

This is a single page app serving a basic version of Glose's bookstore of free books.

This app uses:
* React
* React-Bootstrap


## Instructions

To start the app, download the code, `cd` into the project directory and type:

```bash
npm install
npm start
```

To create a production bundle, use `npm run build`.
